# instructions

build with

```bash
scons --test-coverage
```

command and then generate coverage report with

```bash
./coverage
```

command, then see results in `coverage_report/inex.html`.

## dependencies

`gcovr` package

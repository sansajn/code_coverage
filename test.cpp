#define CATCH_CONFIG_RUNNER
#include <iostream>
#include <catch.hpp>
#include "concurrent_queue.hpp"

using std::cout, std::endl;

TEST_CASE("we can create an empty concurrent queue", 
	"[concurrent_queue]") {

	concurrent_queue<int> q;
	REQUIRE(q.empty());
}

TEST_CASE("we can push and pop elements to the queue", 
	"[concurrent_queue]") {
	
	concurrent_queue<int> q;
	REQUIRE(q.empty());
	
	q.push(2);
	q.push(5);
	REQUIRE(!q.empty());

	int task;
	REQUIRE(q.try_pop(task));
	REQUIRE(q.try_pop(task));
	REQUIRE(q.empty());
}

int main(int argc, char * argv[]) {
	cout << "testing ..." << endl;
	return Catch::Session{}.run(argc, argv);
}
